

## Intro to Data Science - Week 2 (Data Analysis in Python) 

In this week we will introduce to Data wrangling and data analysis in Python  

---
## Class 1: Data Wrangling 

The first class we will cover: 

1. Identify and handle missing values
2. Data Formatting 
3. Data Normalization ( centering/scaling) 
4. Data Binning
5. Tuning categorical variables into numerical 


---

## Class 2 : Data Anslysis 

In this class we will cover: 

1. Distribution
2. Correlation
3. Mean/Max/Average/Median/Mode
4. Moments
5. Visualization of columns and their importance / no importance in the model


---

## Class 3: Lab exercises: 

In this last class of this week we will solve exercises and problems together covering everything that has been learn before.
